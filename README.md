# ProjetINFO3036_TimeTable

Projet INFO 3036


cd existing_repo
git remote add origin https://gitlab.com/vignol/projetinfo3036_timetable.git
git branch -M main
git push -uf origin main
```

#### PACKAGES INSTALL
1. arielmejiadev/larapex-charts: for diagram, charts
2. artesaos/seotools: for Seach Engine Optimization (SEO)
3. barryvdh/laravel-dompdf: to generate PDF file
4. brian2694/laravel-toastr: for Toast notification
5. intervention/image: for image creation while uploading
6. spatie/laravel-sitemap: generating the sitemap of the application
7. jorenvanhocht/laravel-share: to share to social networks

#### HOW TO RUN THE App
1. Clone the repository from github :  https://gitlab.com/vignol/projetinfo3036_timetable.git
2. Install all the necessary packages by: `$ composer install` or `$ composer update `
3. Create the .env file by typing the command:`$ cp .env.example .env`
4. Generate Application key with: `$ php artisan key:generate`
5. Run the migration (create the database): `$ php artisan migrate`
6. Insert dumy data: `$ php artisan db:seed`
7. Run the app: `$ php artisan serve` and launch your browser on the url from your local machine
8. Enjoy !!!
